EIGEN_PATH=/path/to/eigen

all: run-poly

run-poly: poly2.cpp Makefile
	g++ -std=c++17 -O2 -march=native -I${EIGEN_PATH} poly2.cpp -o run-poly

clean:
	rm -rf run-poly
