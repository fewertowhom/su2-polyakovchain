# SU2-PolyakovChain

Simple code to simulate an SU(2) Polyakov Chain using the Metropolis algorithm.
It computes the action for every Markov chain step, outputing each result to a file.
Additionally, at the end of the computation the ensemble average of the action will be printed both to the screen and to a file, as well as an estimate of its statistical error.

## Dependencies
- [Eigen](https://eigen.tuxfamily.org/)

## Installation
Change the path to the Eigen library on ```Makefile```, then
```
make
./run-poly
```

## Run parameters
At the beginning of the ```main``` function inside  ```poly2.cpp``` you will find the following parameters:
```
std::string const fileName = "N10.dat";
int constexpr Nl = 10;
double constexpr beta = 0.90;
```
They correspond to, in this order:
- Name of the file where the average values of the action will be written to;
- Number of links in the chain;
- Value of the coupling.
