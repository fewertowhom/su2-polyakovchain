#include <vector>
#include <random>
#include <complex>
#include <fstream>
#include <iostream>
#include "Eigen/Core"

using mat = Eigen::Matrix2cd;
using vec = Eigen::Vector3d;
int constexpr SEED = 42;
std::complex<double> constexpr I = std::complex<double>{0.0, 1.0};

mat const PauliX = mat{{0.0, 1.0},
						{1.0, 0.0}};
mat const PauliY = mat{{0.0, -I},
						{I, 0.0}};
mat const PauliZ = mat{{1.0, 0.0},
						{0.0, -1.0}};

mat Exponentiate(vec v) {
	auto const norm = v.norm();
	v /= norm;
	auto const Id = mat::Identity();
	return Id * std::cos(norm) + I * (
			v[0] * PauliX + 
			v[1] * PauliY + 
			v[2] * PauliZ) * std::sin(norm);
}

struct Field {
	int const Nl;
	double const beta;
	std::vector<mat> field;

	// random number generation
	std::mt19937 gen;
	std::uniform_real_distribution<> ud;
	std::normal_distribution<> nd;

	Field(double const Beta, int const nl)
	: Nl{nl}
	, beta{Beta}
	, field{std::vector<mat>(Nl)}
	, gen{SEED}
	, ud{0.0, 1.0}
	, nd{0.0, 1.0}
	{
		// random initial configuration;
		for (int i = 0; i < Nl; ++i) {
			auto v = vec{nd(gen), nd(gen), nd(gen)};
			field[i] = Exponentiate(v);
		}
	}

	double Action() const {
		auto tmp = field[0];
		for (int i = 1; i < Nl; ++i) {
			tmp *= field[i];
		}
		return -0.5 * beta * tmp.trace().real();	// because we know the trace IS real
	}

	double MetropolisStep(double const sigma) {
		int rejected = 0;
		
		// sweep over the lattice
		for (int i = 0; i < Nl; ++i) {
			auto const oldAction = Action();
			auto const oldFieldValue = field[i];

			// propose update
			auto const propVec = vec{nd(gen) * sigma, nd(gen) * sigma, nd(gen) * sigma};
			field[i] *= Exponentiate(propVec);		// proposed upgrade guaranteed to be in SU(2)
			auto const dS = Action() - oldAction;
			if (dS > 0) {						// action has increased
				auto const r = ud(gen);
				if (r >= std::exp(-dS))	{		// reject update
					field[i] = oldFieldValue;
					rejected++;
				}
			}
		}
		// if the action has decreased, or if r < exp(-S)
		// the field retains the proposed value

		return 1.0 - (1.0 *rejected) / (Nl); // ratio of accepted proposals in one sweep
	}
};

double StDev(std::vector<double> const& vec) {
	auto const mean = std::accumulate(vec.cbegin(), vec.cend(), 0.0) / vec.size();
	double sum = 0.0;
	for (auto const e : vec)
		sum += (e - mean) * (e - mean);
	sum /= vec.size() - 1;
	return std::sqrt(sum);
}

int main() {
	// output file name
	std::string const fileName = "N10.dat";

	// lattice dimensions
	int constexpr Nl = 10;

	double constexpr beta = 0.90;

	// MC parameters
	int constexpr nTherm = 1000;
	int constexpr nSteps = 1000000;
	int constexpr nSkip  = 100;

	auto phi = Field{beta, Nl};

	auto act = std::vector<double>{};
	act.resize(nSteps / nSkip);

	// thermalise system
	std::cout << "Beginning thermalisation step\n";
	for (int i = 0; i < nTherm; ++i)
		phi.MetropolisStep(1.0);

	std::cout << "# beta = " << beta << '\n';

	std::cout << "Done thermalising!\n";
	double counter = 0.0;
	int j = 0;
	for (int i = 0; i < nSteps; ++i) {
		counter += phi.MetropolisStep(1.0);
		if (i % nSkip == 0) {
			auto const tmp = phi.Action();
			act[j] = tmp;
			j++;
		}
	}

	// average magnetisation
	auto const AVG = std::accumulate(act.cbegin(), act.cend(), 0.0) / act.size();

	std::cout << "# acceptance rate: " << counter / nSteps << '\n'
			  << "# average action:   " << AVG
			  << " +/- " << StDev(act) / std::sqrt(act.size()) << '\n';

	// output to file magnetisation and susceptibility for each MC step
	auto fi = std::ofstream(fileName);
	fi << "# beta  = " << beta << '\n';
	for (int i = 0; i < act.size(); ++i)
		fi << i << '\t' << act[i] << '\n';

	fi << "# acceptance rate: " << counter / nSteps << '\n'
	   << "# average field:   " << AVG
	   << " +/- " << StDev(act) / std::sqrt(act.size()) << '\n';

	fi.close();

	return 0;
}
